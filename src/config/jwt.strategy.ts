import { Inject, Injectable, UnauthorizedException } from "@nestjs/common";
import { ConfigType } from "@nestjs/config";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import baseConfig from "../config/base.config";
import { UserService } from "../user/app/user.service";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @Inject(baseConfig.KEY)
    bsConfig: ConfigType<typeof baseConfig>,
    private usersService: UserService
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: bsConfig.jwtScretKey,
    });
  }

  async validate(payload: any) {
    const user = await this.usersService.findOneByEmail(payload.email);
    if (!user) {
      throw new UnauthorizedException(
        "El token no es válido, inicie sesión para continuar"
      );
    }
    return this.usersService.hideDefaultParameters(user);
  }
}
