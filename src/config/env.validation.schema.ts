import * as Joi from 'joi';
import {
  APP_NAME,
  APP_URL,
  PORT,
  DATABASE_URL,
  JWT_SECRET_KEY,
  JWT_EXPIRATION_TIME,
  JWT_EXPIRATION_TIME_RECOVER_PASSWORD,   
} from './globalVariables';

const validationSchema = Joi.object({ 
  [APP_NAME]: Joi.string().required(),
  [APP_URL]: Joi.string().required(),
  [PORT]: Joi.number().required(),  
  [DATABASE_URL]: Joi.string().required(), 
  [JWT_SECRET_KEY]: Joi.string().required(),
  [JWT_EXPIRATION_TIME]: Joi.string().required(),
  [JWT_EXPIRATION_TIME_RECOVER_PASSWORD]: Joi.string().required(), 
});

export default validationSchema;
