import { registerAs } from '@nestjs/config';
import {
  APP_NAME,
  APP_URL, 
  PORT,
  DATABASE_URL,
  JWT_SECRET_KEY,
  JWT_EXPIRATION_TIME,
  JWT_EXPIRATION_TIME_RECOVER_PASSWORD, 
} from './globalVariables';

export default registerAs('base', () => ({
  appName: process.env[APP_NAME],
  appUrl: process.env[APP_URL], 
  port: process.env[PORT],
  databaseUrl: process.env[DATABASE_URL],
  jwtScretKey: process.env[JWT_SECRET_KEY],
  jwtExpirationTime: process.env[JWT_EXPIRATION_TIME],
  jwtExpirationTimeRecoverPassword:
    process.env[JWT_EXPIRATION_TIME_RECOVER_PASSWORD], 
}));
