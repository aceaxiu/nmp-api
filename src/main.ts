import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import { PrismaService } from './prismaModule/prisma.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const prismaService: PrismaService = app.get(PrismaService); 

  const config = new DocumentBuilder()
  .setTitle(AppModule.appName)
  .setDescription('Set of routes for different user groups')
  .setVersion('1.0')
  .addServer(`${AppModule.appUrl}`)
  .addBearerAuth()
  .build();
const document = SwaggerModule.createDocument(app, config);

SwaggerModule.setup(`api/docs`, app, document);

app.useGlobalPipes(new ValidationPipe({ whitelist: true }));
app.enableCors();

const runningPort = process.env.PORT || AppModule.port;
await app.listen(runningPort, () => {
  console.log(`[SERVER RUNING ON ${runningPort}]`);
});
}
bootstrap();
