import { Module } from "@nestjs/common";
import { POKEMON_REPOSITORY } from "src/shared/domain/repositories/IPokemonRepository";
import { PokemonRepository } from "src/shared/infra/repositories/PokemonRepository";
import { PrismaModule } from "../prismaModule/prisma.module";
import { PokemonService } from "./app/pokemon.service";
import { POKEMON_SERVICE } from "./domain/IPokemonService";
import { PokemonController } from "./infra/pokemon.controller";

@Module({
  imports: [PrismaModule],
  controllers: [PokemonController],
  providers: [
    {
      provide: POKEMON_SERVICE,
      useClass: PokemonService,
    },
    {
      provide: POKEMON_REPOSITORY,
      useClass: PokemonRepository,
    },
    PokemonService,
  ],
  exports: [PokemonService],
})
export class PokemonModule {}
