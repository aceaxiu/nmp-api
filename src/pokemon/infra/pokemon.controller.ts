import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Inject,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from "@nestjs/common";
import { ApiBearerAuth, ApiResponse, ApiTags } from "@nestjs/swagger";
import { JwtAuthGuard } from "src/config/jwt-auth.guard";
import type { IPokemon } from "../domain/IPokemon";
import { IPokemonService, POKEMON_SERVICE } from "../domain/IPokemonService";
import { CreatePokemonDto } from "../domain/dto/createPokemons.dto";
import { PokemonFiltersDto } from "../domain/dto/pokemonFilters.dto";
import { UpdatePokemonDto } from "../domain/dto/updatePokemons.dto";

@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller({
  path: `api/pokemon`,
})
export class PokemonController {
  constructor(
    @Inject(POKEMON_SERVICE)
    private pokemonService: IPokemonService
  ) {}

  @ApiTags("Pokemon")
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: "Returns pokemon object",
  })
  @Post()
  async create(@Body() data: CreatePokemonDto): Promise<IPokemon> {
    const pokemon = await this.pokemonService.create(data);
    return pokemon;
  }

  @ApiTags("Pokemon")
  @ApiResponse({
    status: HttpStatus.OK,
    description: "Returns pokemon object",
  })
  @Get()
  async findAll(@Query() query: PokemonFiltersDto): Promise<IPokemon[]> {
    return await this.pokemonService.findAll(query);
  }

  @ApiTags("Pokemon")
  @ApiResponse({
    status: HttpStatus.OK,
    description: "Returns pokemon object",
  })
  @Get("/user/:userId")
  async findAllUser(@Param("userId") userId: string): Promise<IPokemon[]> {
    return await this.pokemonService.findAllUser(userId);
  }

  @ApiTags("Pokemon")
  @ApiResponse({
    status: HttpStatus.OK,
    description: "Returns pokemon list",
  })
  @Get("list")
  async getPokemonList(@Query() query: any): Promise<any> {
    return await this.pokemonService.getPokemonList(query);
  }

  @ApiTags("Pokemon")
  @ApiResponse({
    status: HttpStatus.OK,
    description: "Returns pokemon list",
  })
  @Get("filter")
  async getPokemonFilter(@Query() query: any): Promise<any> {
    return await this.pokemonService.getPokemonFilter(query);
  }

  @ApiTags("Pokemon")
  @ApiResponse({
    status: HttpStatus.OK,
    description: "Returns pokemon object",
  })
  @Get(":id")
  async findOne(@Param("id") id: string): Promise<IPokemon> {
    return await this.pokemonService.findOne(id);
  }

  @ApiTags("Pokemon")
  @ApiResponse({
    status: HttpStatus.OK,
    description: "Returns pokemon object",
  })
  @Put(":id")
  async update(
    @Param("id") id: string,
    @Body() data: UpdatePokemonDto
  ): Promise<IPokemon> {
    const pokemon = await this.pokemonService.update(data);
    return pokemon;
  }

  @ApiTags("Pokemon")
  @ApiResponse({
    status: HttpStatus.OK,
    description: "Returns pokemon object",
  })
  @Delete(":id")
  async delete(@Param("id") id: string): Promise<IPokemon> {
    return await this.pokemonService.delete(id);
  }
}
