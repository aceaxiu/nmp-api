import { Inject, Injectable, NotFoundException, BadRequestException } from "@nestjs/common";
import { ObjectId } from "mongodb";
import {
  IPokemonRepository,
  POKEMON_REPOSITORY,
} from "../../shared/domain/repositories/IPokemonRepository";
import type { IPokemon } from "../domain/IPokemon";
import type { IPokemonService } from "../domain/IPokemonService";

@Injectable()
export class PokemonService implements IPokemonService {
  constructor(
    @Inject(POKEMON_REPOSITORY)
    private readonly pokemonRepository: IPokemonRepository
  ) {}

  async create(data: any): Promise<any> {
    try{
      const { userId, pokemons } = data;
      const pokedex = []
      const errors = []
      const created = await Promise.all(
        pokemons.map(async (p: string) => {
          const poke = await this.getPokemonFilter({ name: p });
          if ((poke.status && poke.status==404)||(poke.status && poke.status !== 200)) {
            errors.push(p)
          }else{
            const { id, pokemon, image } = poke;
            const created =  await this.pokemonRepository.create({
                userId,
                pokemonId: String(id),
                pokemon,
                image,
              });
              pokedex.push(created)
            }
        })
      );

      return {pokedex, errors};
    }catch (error) {
      return error
    }
  }

  async findAllUser(query): Promise<IPokemon[]> {
    return await this.pokemonRepository.findAllUser(query);
  }

  async findAll(query): Promise<IPokemon[]> {
    return await this.pokemonRepository.findAll(query, "11");
  }

  async findAllSimple(query): Promise<IPokemon[]> {
    return await this.pokemonRepository.findAllSimple(query);
  }

  async findOne(id): Promise<IPokemon> {
    if (!ObjectId.isValid(id)) {
      throw new NotFoundException("ID de material no valido");
    }

    return await this.pokemonRepository.findOne(id);
  }

  async update(data: any): Promise<any> {
    try{
      const { userId, oldPokemons, pokemons } = data;
      const pokedex = []
      const errors = []
      const updated = await Promise.all(
        pokemons.map(async (p: string) => {
          const poke = await this.getPokemonFilter({ name: p });
          if ((poke.status && poke.status==404)||(poke.status && poke.status !== 200)) {
            errors.push(p)
          }else{
            const { id, pokemon, image } = poke;
            const created =  await this.pokemonRepository.create({
                userId,
                pokemonId: String(id),
                pokemon,
                image,
              });
              pokedex.push(created)
            }
        })
      );

      await Promise.all(
        oldPokemons.map(async (p: string) => {
          try{
            const findPokemon = await this.pokemonRepository.findOne(p);
            await this.pokemonRepository.delete(p);
          }catch(error){
          }
          
        })
      );

      return {pokedex, errors};;
    }catch (error) {
      console.log("Error", error)
      return error
    }
   
  }

  async delete(id): Promise<IPokemon> {
    return await this.pokemonRepository.delete(id);
  }

  async getPokemonList(query: any): Promise<any> {
    try {
      const response: any = await fetch(
        `https://pokeapi.co/api/v2/pokemon?offset=${query.offset}&limit=${query.limit}`
      );
      if (!response.ok) {
        throw new Error("Error al obtener la lista de Pokémon");
      }

      const pokemonList = await response.json();

      const pokemons = await Promise.all(
        pokemonList.results.map(async (p: any) => {
          const response = await fetch(p.url);
          const poke = await response.json();

          return {
            id: poke.id,
            pokemon: p.name,
            image: poke.sprites.other.dream_world.front_default,
          };
        })
      );

      return {
        next: pokemonList.next.split("?")[1],
        previous: pokemonList.previous
          ? pokemonList.previous.split("?")[1]
          : null,
        pokemons,
      };
    } catch (error) {
      throw new Error("Error al obtener la lista de Pokémon");
    }
  }

  async getPokemonFilter(query: any): Promise<any> {
    try {
      const response: any = await fetch(
        `https://pokeapi.co/api/v2/pokemon/${query.name}`
      );
      if (response.status === 404)
         throw new NotFoundException("No se encontro el pokemon")
      else if(response.status !== 200)throw new BadRequestException('Error en el servicio de pokeapi');
      

      const pokemon = await response.json();

      return {
        id: pokemon.id,
        pokemon: pokemon.name,
        image: pokemon.sprites.other.dream_world.front_default,
      };
    } catch (error) {
      return error
    }
  }
}
