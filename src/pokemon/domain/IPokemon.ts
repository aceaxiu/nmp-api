export interface IPokemon {
  pokemonId: string;
  pokemon: string;
  image: string;
  userId: string;
}
