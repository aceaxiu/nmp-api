import { ApiProperty } from "@nestjs/swagger";
import { IsIn, IsOptional } from "class-validator";

export class PokemonFiltersDto {
  @ApiProperty()
  @IsOptional()
  @IsIn(["asc", "desc"], {
    message:
      "El campo $property debe ser uno de los siguientes valores: $constraint1",
  })
  readonly sort: string;

  @ApiProperty()
  @IsOptional()
  readonly perPage: string;

  @ApiProperty()
  @IsOptional()
  readonly page: string;
}
