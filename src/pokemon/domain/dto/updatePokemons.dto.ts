import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class UpdatePokemonDto {
  @ApiProperty()
  @IsNotEmpty()
  oldPokemons: string[];

  @ApiProperty()
  @IsNotEmpty()
  pokemons: string[];

  @ApiProperty()
  @IsNotEmpty()
  userId: string;
}
