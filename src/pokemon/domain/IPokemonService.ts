import { IPokemon } from "./IPokemon";

export const POKEMON_SERVICE = Symbol("IPokemonService");

export interface IPokemonService {
  create(data: any): Promise<any>;
  findAllUser(query: any): Promise<IPokemon[]>;
  findAll(query: any): Promise<IPokemon[]>;
  findAllSimple(query: any): Promise<IPokemon[]>;
  findOne(id: any): Promise<IPokemon>;
  update(data: any): Promise<any>;
  delete(id: any): Promise<IPokemon>;
  getPokemonList(query: any): Promise<any>;
  getPokemonFilter(query: any): Promise<any>;
}
