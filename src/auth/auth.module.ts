import { Module } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { JwtModule } from "@nestjs/jwt";
import { PassportModule } from "@nestjs/passport";
import { JWT_EXPIRATION_TIME, JWT_SECRET_KEY } from "../config/globalVariables";
import { JwtStrategy } from "../config/jwt.strategy";
import { LocalStrategy } from "../config/local.strategy";
import { UserModule } from "../user/user.module";
import { AuthService } from "./auth.service";

@Module({
  imports: [
    UserModule,
    PassportModule,
    JwtModule.registerAsync({
      useFactory: (config: ConfigService) => {
        const secret = config.get(JWT_SECRET_KEY);
        const expirationTime = config.get(JWT_EXPIRATION_TIME);
        return {
          secret: secret,
          signOptions: {
            expiresIn: expirationTime,
          },
        };
      },
      inject: [ConfigService],
    }),
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}
