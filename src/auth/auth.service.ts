import {
  BadRequestException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from "@nestjs/common";
import { ConfigType } from "@nestjs/config";
import { JwtService } from "@nestjs/jwt";
import { User as UserModel } from "@prisma/client";
import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";
import baseConfig from "../config/base.config";
import { UserService } from "../user/app/user.service";

@Injectable()
export class AuthService {
  constructor(
    @Inject(forwardRef(() => UserService))
    private usersService: UserService,
    private jwtService: JwtService,
    @Inject(baseConfig.KEY)
    private readonly bsConfig: ConfigType<typeof baseConfig>
  ) {}

  async validateUser(email: string, pass: string): Promise<any> {
    const user = await this.usersService.findOneByEmail(email);
    if (!user) {
      throw new NotFoundException("Usuario no encontrado");
    }
    if (!user.password) {
      throw new UnauthorizedException(
        "El usuario no cuenta con una contraseña asignada"
      );
    }
    const isPasswordMatching = await bcrypt.compare(pass, user.password);
    if (!isPasswordMatching) {
      throw new UnauthorizedException("La contraseña no coincide");
    }
    return this.usersService.hideDefaultParameters(user);
  }

  async generateTokenByUser(user: UserModel) {
    const payload = { email: user.email, sub: user.uuid, id: user.id };
    return await this.jwtService.sign(payload);
  }

  async signTokenToRecoverPassword(user: UserModel) {
    const payload = { email: user.email, sub: user.uuid, id: user.id };
    const token = await this.jwtService.sign(payload, {
      expiresIn: this.bsConfig.jwtExpirationTimeRecoverPassword,
    });
    return token;
  }

  async checkTokenJWT(token: string): Promise<any> {
    try {
      const decoded = await jwt.verify(token, this.bsConfig.jwtScretKey);
      return decoded;
    } catch (error) {
      throw new BadRequestException("La URL ha expirado");
    }
  }
}
