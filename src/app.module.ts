import { Inject, Module } from "@nestjs/common";
import { ConfigModule, ConfigType } from "@nestjs/config";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { AuthModule } from "./auth/auth.module";
import baseConfig from "./config/base.config";
import validationSchema from "./config/env.validation.schema";
import { PokemonModule } from "./pokemon/pokemon.module";
import { PrismaModule } from "./prismaModule/prisma.module";

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: ".env",
      load: [baseConfig],
      validationSchema,
    }),
    PrismaModule,
    AuthModule,
    PokemonModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  static appName: string;
  static port: number | string;
  static appUrl: string;
  constructor(
    @Inject(baseConfig.KEY)
    private readonly bsConfig: ConfigType<typeof baseConfig>
  ) {
    AppModule.appName = this.bsConfig.appName;
    AppModule.port = this.bsConfig.port;
    AppModule.appUrl = this.bsConfig.appUrl;
  }
}
