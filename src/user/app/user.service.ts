import {
  forwardRef,
  Inject,
  Injectable,
  InternalServerErrorException,
  BadRequestException
} from "@nestjs/common";
import * as bcrypt from "bcrypt";
import { AuthService } from "../../auth/auth.service";
import {
  IUserRepository,
  USER_REPOSITORY,
} from "../../shared/domain/repositories/IUserRepository";
import type { IUser } from "../domain/IUser";
import type { IUserService } from "../domain/IUserService";

@Injectable()
export class UserService implements IUserService {
  constructor(
    @Inject(USER_REPOSITORY)
    private readonly userRepository: IUserRepository,
    @Inject(forwardRef(() => AuthService))
    private authService: AuthService
  ) {}

  async createUser(data: any) {
    const { password, email, ...user } = data;
    const pass = await bcrypt.hash(password, 10);
    const verifyEmail = await this.findOneByEmail(email)
    if(verifyEmail) throw new BadRequestException('El correo se encuentra registrado'); 
    const userCreatede = await this.userRepository.createUser({
      password: pass,
      email,
      ...user,
    });
    return userCreatede;
  }

  async findAll(query: any): Promise<IUser[]> {
    return await this.userRepository.findAll(query);
  }

  async findOne(id: string): Promise<IUser> {
    return await this.userRepository.findOne(id);
  }

  async findOneByEmail(email: string): Promise<IUser> {
    return await this.userRepository.findOneByEmail(email);
  }

  async findOneByPhone(phone: string): Promise<IUser> {
    return await this.userRepository.findOneByPhone(phone);
  }

  async findOneByUuid(uuid: string): Promise<IUser> {
    return await this.userRepository.findOneByUuid(uuid);
  }

  async confirmUser(token: string, res: any): Promise<IUser> {
    const tokenData = await this.authService.checkTokenJWT(token);
    const user: any = await this.userRepository.findOne(tokenData.id);

    if (!user) {
      return res.render("verifyEmail", {
        title: "¡Error!",
        message: "Error al verificar correo electrónico, token no válido.",
        type: "error",
        layout: false,
      });
    }
    if (user.emailVerified) {
      return res.render("verifyEmail", {
        title: "¡Error!",
        message: "La cuenta ya se encuentra verificada.",
        type: "error",
        layout: false,
      });
    }

    await this.userRepository.confirmUser(tokenData.id);
    return res.render("verifyEmail", {
      title: "¡Cuenta verificada!",
      message: "Tu correo electrónico ha sido verificado exitosamente.",
      type: "success",
      layout: false,
    });
  }

  async deleteUser(id: string) {
    const user = await this.userRepository.deleteUser(id);
    if (!user) throw new InternalServerErrorException("Usuario no encontrado");
    return user;
  }

  async changeStatus(id: string, data: any) {
    const user = await this.userRepository.changeStatus(id, data);
    if (!user) throw new InternalServerErrorException("Usuario no encontrado");
    return user;
  }

  async updateUser(userId: string, data: any) {
    return await this.userRepository.updateUser(userId, data);
  }

  async findOneByEmailNotId(userId: string, data: any) {
    return await this.userRepository.findOneByEmailNotId(userId, data);
  }
  async findOneByPhoneNotId(userId: string, data: any) {
    return await this.userRepository.findOneByPhoneNotId(userId, data);
  }

  async updateUserInformation(userId: string, data: any) {
    if (data.email) {
      const verifyEmail = await this.userRepository.findOneByEmailNotId(
        userId,
        data.email
      );
      if (verifyEmail)
        throw new InternalServerErrorException(
          "El correo electrónico ya se encuentra registrado"
        );
    }

    const user: any = await this.userRepository.findOne(userId);
    data.fullName = data.firstName + data.lastName;
    if (data.password) {
      const hash = await bcrypt.hash(data.password, 10);
      data.password = hash;
    } else {
      delete data.password;
    }
    if (data.email && user.email !== data.email) {
      data.emailVerified = false;
      await this.userRepository.updateUser(userId, data);

      return {
        displayMessage: `Se ha enviado un email de confirmación al correo electrónico ${data.email}`,
      };
    } else {
      await this.userRepository.updateUser(userId, data);
      return { displayMessage: "Se actualizo la información del usuario" };
    }
  }

  hideDefaultParameters(user: any): Promise<IUser> {
    const { password, ...result } = user;
    return result;
  }
}
