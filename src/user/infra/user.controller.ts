import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Inject,
  Param,
  Post,
  Put,
  Query,
  Request,
  UseGuards,
} from "@nestjs/common";
import {
  ApiBearerAuth,
  ApiExcludeEndpoint,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import { AuthService } from "../../auth/auth.service";
import { JwtAuthGuard } from "../../config/jwt-auth.guard";
import { LocalAuthGuard } from "../../config/local-auth.guard";
import type { IUser } from "../domain/IUser";
import { IUserService, USER_SERVICE } from "../domain/IUserService";
import { FilterUSerDto } from "../domain/dto/filter.dto";
import { LoginDto } from "../domain/dto/login.dto";
import { ProfileDto } from "../domain/dto/profile.dto";
import { UserRegisterDto } from "../domain/dto/userRegister.dto";

@Controller({
  path: `api`,
})
export class UserController {
  constructor(
    @Inject(USER_SERVICE)
    private userService: IUserService,
    private authService: AuthService
  ) {}

  @ApiBearerAuth()
  @ApiExcludeEndpoint()
  @Get("users")
  async findAll(@Query() query: FilterUSerDto): Promise<IUser[]> {
    return await this.userService.findAll(query);
  }

  @ApiBearerAuth()
  @ApiExcludeEndpoint()
  @Get("user/:id")
  async findOne(@Param("id") id: string): Promise<IUser> {
    return await this.userService.findOne(id);
  }

  @ApiTags("Auth")
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: "Returns user object and token",
  })
  @UseGuards(LocalAuthGuard)
  @Post("auth/login")
  async login(@Body() body: LoginDto, @Request() req: any) {
    const { user } = req;
    const token = await this.authService.generateTokenByUser(user);
    return {
      user: user,
      access_token: token,
    };
  }

  @ApiTags("Auth")
  @Post("auth/sig-in")
  async sigIn(@Body() body: UserRegisterDto) {
    const user = await this.userService.createUser(body);
    return user;
  }

  @ApiTags("Auth")
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.OK,
    description: "Returns user object",
  })
  @Get("auth/me")
  @UseGuards(JwtAuthGuard)
  async get(@Request() req) {
    const { user } = req;
    return user;
  }

  @ApiBearerAuth()
  @ApiTags("User")
  @UseGuards(JwtAuthGuard)
  @Put("user/:id")
  async update(
    @Param("id") id: string,
    @Body() body: ProfileDto
  ): Promise<any> {
    const user = await this.userService.updateUser(id, body);
    return user;
  }

  @ApiBearerAuth()
  @ApiTags("User")
  @UseGuards(JwtAuthGuard)
  @Delete("user/:id")
  async delete(@Param("id") id: string): Promise<any> {
    const user = await this.userService.deleteUser(id);
    return user;
  }
}
