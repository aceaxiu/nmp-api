import { forwardRef } from "@nestjs/common";
import { Test, TestingModule } from "@nestjs/testing";
import { AuthModule } from "../../auth/auth.module";
import { PrismaModule } from "../../prismaModule/prisma.module";
import { UserRepository } from "../../shared/infra/repositories/UserRepository";
import { UserRegisterDto } from "../domain/dto/userRegister.dto";
import { UserController } from "./user.controller";

describe("UserControlle", () => {
  let userController: UserController;
  let userRepository: UserRepository;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [PrismaModule, forwardRef(() => AuthModule)],
      controllers: [UserController],
      providers: [UserRepository],
    }).compile();

    userController = app.get<UserController>(UserController);
  });

  // describe("login", () => {
  //   it('should return "Hello World!"', async () => {
  //     const data: LoginDto = { email: "jasson@gmail.com", password: "" };
  //     const result = await userController.login(data, { user: {} });
  //     console.log("🚀 ~ it ~ result:", result);
  //     expect(result).toBeDefined();
  //   });
  // });

  describe("sigIn", () => {
    it('should return "Sig In"-->>>', async () => {
      const data: UserRegisterDto = {
        email: "jasson@acea.com",
        fullName: "Demo User",
        firstName: "Test",
        lastName: "User",
        phone: "1234567890",
        passsword: "123456789",
      };
      const result = await userController.sigIn(data);
      console.log("🚀 ~ it ~ result:", result);
      expect(result).toBeDefined();
    });
  });
});
