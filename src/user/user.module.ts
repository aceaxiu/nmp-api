import { forwardRef, Module } from "@nestjs/common";
import { AuthModule } from "../auth/auth.module";
import { PrismaModule } from "../prismaModule/prisma.module";
import { USER_REPOSITORY } from "../shared/domain/repositories/IUserRepository";
import { UserRepository } from "../shared/infra/repositories/UserRepository";
import { UserService } from "./app/user.service";
import { USER_SERVICE } from "./domain/IUserService";
import { UserController } from "./infra/user.controller";

@Module({
  imports: [PrismaModule, forwardRef(() => AuthModule)],
  controllers: [UserController],
  providers: [
    {
      provide: USER_SERVICE,
      useClass: UserService,
    },
    {
      provide: USER_REPOSITORY,
      useClass: UserRepository,
    },
    UserService,
  ],
  exports: [UserService],
})
export class UserModule {}
