export interface IUser {
  fullName: string;
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  phone: string;
}
