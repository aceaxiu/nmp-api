import { ApiProperty } from "@nestjs/swagger";
import { IsOptional } from "class-validator";

export class FilterUSerDto {
  @ApiProperty()
  @IsOptional()
  name: string;

  @ApiProperty()
  @IsOptional()
  email: string;

  @ApiProperty()
  @IsOptional()
  pokemon: string;
}
