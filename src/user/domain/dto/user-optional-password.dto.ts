import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsPhoneNumber,
  IsString,
  MinLength,
} from 'class-validator';

export class UserOptionalPasswordCommand {
  @ApiProperty()
  @IsNotEmpty()
  firstName: string;

  @ApiProperty()
  @IsNotEmpty()
  lastName: string;

  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsPhoneNumber('MX')
  phone?: string;

  @ApiProperty()
  @IsOptional()
  @IsString({
    message: 'El campo password es requerido',
  })
  @MinLength(8, {
    message: 'El campo contraseña debe ser de al menos 8 caracteres',
  })
  readonly password: string;
}
