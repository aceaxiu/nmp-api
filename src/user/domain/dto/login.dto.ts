import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty } from "class-validator";

export class LoginDto {
  @ApiProperty()
  @IsNotEmpty({
    message: "El campo email es requerido",
  })
  @IsEmail(
    {},
    {
      message: "El campo email debe ser un correo electrónico valido",
    }
  )
  readonly email: string;

  @ApiProperty()
  @IsNotEmpty({
    message: "El campo password es requerido",
  })
  readonly password: string;
}
