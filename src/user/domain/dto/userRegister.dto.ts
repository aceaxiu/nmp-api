import { ApiProperty } from "@nestjs/swagger";
import {
  IsNotEmpty,
  IsNumberString,
  IsOptional,
  MaxLength,
  MinLength,
} from "class-validator";

export class UserRegisterDto {
  @ApiProperty()
  @IsNotEmpty()
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  fullName: string;

  @ApiProperty()
  @IsNotEmpty()
  firstName: string;

  @ApiProperty()
  @IsOptional()
  lastName: string;

  @ApiProperty()
  @IsNotEmpty({
    message: "El campo telefono es requerido",
  })
  @IsNumberString(
    { no_symbols: false },
    { message: `El campo telefono debe ser de 13 dígitos` }
  )
  @MinLength(10, { message: "El campo telefono debe ser minimo de 10 digitos" })
  @MaxLength(13, { message: "El campo telefono debe ser maximo de 13 digitos" })
  phone: string;

  @ApiProperty()
  @IsNotEmpty()
  password: string;
}
