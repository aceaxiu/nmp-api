import { ApiProperty } from "@nestjs/swagger";
import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsPhoneNumber,
  MinLength,
} from "class-validator";
import { Match } from "../../../config/match.decorator";

export class UserCommand {
  @IsNotEmpty()
  firstName: string;
  @IsNotEmpty()
  lastName: string;
  @IsEmail()
  @IsNotEmpty()
  email: string;
  @ApiProperty()
  @IsNotEmpty({
    message: "El campo password es requerido",
  })
  @MinLength(8, {
    message: "El campo contraseña debe ser de al menos 8 caracteres",
  })
  readonly password: string;
  @ApiProperty()
  @IsOptional()
  @IsNotEmpty()
  @IsPhoneNumber("MX")
  phone?: string;
  @ApiProperty()
  @IsNotEmpty({
    message: "El campo confirmación de contraseña es requerido",
  })
  @Match("password")
  readonly passwordConfirmation: string;
}
