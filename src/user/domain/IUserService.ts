import { IUser } from "./IUser";

export const USER_SERVICE = Symbol("IUserService");

export interface IUserService {
  createUser(data: any): Promise<any>;
  findAll(query: any): Promise<IUser[]>;
  findOne(id: string): Promise<IUser>;
  findOneByEmail(email: string): Promise<IUser>;
  findOneByPhone(phone: string): Promise<IUser>;
  findOneByUuid(uuid: string): Promise<IUser>;
  confirmUser(token: string, res: any): Promise<IUser>;
  deleteUser(id: string): Promise<any>;
  changeStatus(id: string, data: any): Promise<any>;
  updateUser(userId: string, data: any): Promise<any>;
  findOneByEmailNotId(userId: string, data: any): Promise<any>;
  findOneByPhoneNotId(userId: string, data: any): Promise<any>;
  updateUserInformation(userId: string, data: any): Promise<any>;
}
