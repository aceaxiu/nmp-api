import { Module } from "@nestjs/common";
import { PrismaService } from "../prismaModule/prisma.service";

@Module({
  providers: [PrismaService],
  exports: [PrismaService],
})
export class PrismaModule {}
