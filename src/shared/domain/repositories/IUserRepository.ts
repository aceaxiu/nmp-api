import { IUser } from "../../../user/domain/IUser";

export const USER_REPOSITORY = Symbol("IUserRepository");

export interface IUserRepository {
  createUser(data: any): Promise<IUser>;

  findAll(query: any): Promise<IUser[]>;

  findOne(id: string): Promise<IUser>;

  findOneByEmail(email: string): Promise<IUser>;

  findOneByPhone(phone: string): Promise<IUser>;

  findOneByUuid(uuid: string): Promise<IUser>;

  confirmUser(uuid: string): Promise<IUser>;

  deleteUser(id: string): Promise<any>;

  changeStatus(id: string, adminStatusCommand: any): Promise<any>;

  updateUser(userId: string, data: any): Promise<IUser>;

  findOneByEmailNotId(userId: string, data: any): Promise<IUser>;

  findOneByPhoneNotId(userId: string, data: any): Promise<IUser>;
}
