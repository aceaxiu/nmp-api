import { IPokemon } from "../../../pokemon/domain/IPokemon";

export const POKEMON_REPOSITORY = Symbol("PokemonRepository");

export interface IPokemonRepository {
  create(pokemonModel: IPokemon): Promise<IPokemon>;

  findAll(query: any, defaultPaginate: string): Promise<any>;

  findAllSimple(query: any): Promise<any>;

  findAllUser(query: any): Promise<any>;

  findOne(id: string): Promise<IPokemon>;

  update(id: string, pokemonModel: IPokemon): Promise<IPokemon>;

  delete(id: string): Promise<IPokemon>;
}
