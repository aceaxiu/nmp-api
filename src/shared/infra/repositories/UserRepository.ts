import { Injectable } from "@nestjs/common";
import { PrismaService } from "../../../prismaModule/prisma.service";
import { IUserRepository } from "../../../shared/domain/repositories/IUserRepository";
import { IUser } from "../../../user/domain/IUser";

@Injectable()
export class UserRepository implements IUserRepository {
  constructor(private readonly prismaService: PrismaService) {}

  async createUser(user: any): Promise<IUser> {
    const userCreated = await this.prismaService.user.create({ data: user });
    return userCreated;
  }

  async findAll(query: any): Promise<IUser[]> {
    const filter: any = {};
    if (query.email) filter.email = query.email;
    if (query.pokemon) filter.pokemon = query.pokemon;
    const list = await this.prismaService.user.findMany({
      include: { pokemons: true },
      where: filter,
    });
    return list;
  }

  async findOne(id: string): Promise<IUser> {
    const user = await this.prismaService.user.findFirst({
      where: { id: id },
    });
    return user;
  }

  async findOneByEmail(email: string): Promise<IUser> {
    const user = await this.prismaService.user.findFirst({
      where: { email: email },
    });
    return user;
  }

  async findOneByPhone(phone: string): Promise<IUser> {
    const user = await this.prismaService.user.findFirst({
      where: { phone: phone },
    });
    return user;
  }
  async findOneByPhoneNotId(id: string, phone: string): Promise<IUser> {
    const user = await this.prismaService.user.findFirst({
      where: { phone: phone, NOT: { id: id } },
    });
    return user;
  }

  async findOneByUuid(uuid: string): Promise<IUser> {
    const user = await this.prismaService.user.findFirst({
      where: { uuid: uuid },
    });
    return user;
  }

  async deleteUser(id: string) {
    const user = await this.prismaService.user.delete({
      where: { id: id },
    });
    return user;
  }

  async changeStatus(id: string, data: any) {
    const user = await this.prismaService.user.update({
      where: { id: id },
      data: data,
    });
    return user;
  }

  async updateUser(userId: string, data: any): Promise<IUser> {
    return await this.prismaService.user.update({
      where: { id: userId },
      data: data,
    });
  }

  async findOneByEmailNotId(id: string, email: string): Promise<IUser> {
    const user = await this.prismaService.user.findFirst({
      where: { email: email, NOT: { id: id } },
    });
    return user;
  }

  async confirmUser(id: string): Promise<IUser> {
    const user = await this.prismaService.user.update({
      where: { id: id },
      data: { emailVerified: true },
    });
    return user;
  }
}
