import { Injectable, NotFoundException } from "@nestjs/common";
import { createPaginator } from "prisma-pagination";
import { IPokemonRepository } from "src/shared/domain/repositories/IPokemonRepository";
import { IPokemon } from "../../../pokemon/domain/IPokemon";
import { PrismaService } from "../../../prismaModule/prisma.service";

@Injectable()
export class PokemonRepository implements IPokemonRepository {
  constructor(private readonly prismaService: PrismaService) {}

  async create(pokemon: IPokemon): Promise<IPokemon> {
    const data = await this.prismaService.pokemon.create({
      data: pokemon,
    });
    return data;
  }

  async findAll(params: any, defaultPaginate: any) {
    // eslint-disable-next-line prefer-const
    let { perPage, page, sort } = params;

    const paginate = createPaginator({
      perPage: perPage ? perPage : defaultPaginate,
    });
    if (!sort) {
      sort = "desc";
    }
    if (!page) {
      page = 1;
    }
    const queryBuilder: any = {
      where: {},
      orderBy: [
        {
          gramPrice: sort,
        },
      ],
    };
    const result = await paginate(this.prismaService.pokemon, queryBuilder, {
      page: parseInt(page),
    });
    return result;
  }

  async findAllUser(params: any) {
    const result = await this.prismaService.pokemon.findMany({
      where: { userId: params },
    });

    return result;
  }

  async findAllSimple(params: any) {
    const result = await this.prismaService.pokemon.findMany(params);

    return result;
  }

  async findOne(id: string): Promise<IPokemon> {
    const pokemon = await this.prismaService.pokemon.findFirst({
      where: { id: id },
    });

    if (!pokemon) {
      throw new NotFoundException("Material no encontrado");
    }

    return pokemon;
  }

  async update(id: string, pokemon: IPokemon): Promise<IPokemon> {
    const data = await this.prismaService.pokemon.update({
      where: { id: id },
      data: pokemon,
    });
    return data;
  }

  async delete(id: string): Promise<IPokemon> {
    const pokemon = await this.prismaService.pokemon.delete({
      where: { id: id },
    });
    return pokemon;
  }
}
