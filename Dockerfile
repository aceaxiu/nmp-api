FROM node:20 AS builder

WORKDIR /app

COPY package*.json ./
COPY prisma ./prisma/

RUN npm install --global glob rimraf

RUN npm install --legacy-peer-deps

RUN npm install webpack --save-dev

RUN npm install prisma -g

RUN prisma generate

COPY . .

RUN npm run build

CMD [ "npm", "run", "start:dev" ]