import { PrismaClient } from '@prisma/client'; 
import { mainUser } from './seeds/user';

const prisma = new PrismaClient();
async function main() { 
  await mainUser();
}
main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
