import { PrismaClient } from "@prisma/client";
import * as bcrypt from "bcrypt";
const prisma = new PrismaClient();
export async function mainUser() {
  console.log("*************************************************");
  console.log("INIT users");
  const password = await bcrypt.hash("123456789", 10);
  const users = [
    {
      where: { email: "test@gmail.com" },
      update: {},
      create: {
        email: "test@gmail.com",
        password: password,
        fullName: "Test ",
        firstName: "Test",
        lastName: "Test",
        isAdmin: false,
        emailVerified: true,
      },
    },
    {
      where: { email: "admin@gmail.com" },
      update: {},
      create: {
        email: "admin@gmail.com",
        password: password,
        fullName: "Administrator",
        firstName: "",
        lastName: "",
        isAdmin: true,
        emailVerified: true,
      },
    },
  ];

  await Promise.all(
    users.map(async (user) => {
      await prisma.user.upsert(user);
    })
  );
  console.log("END users");
  console.log("*************************************************");
}
