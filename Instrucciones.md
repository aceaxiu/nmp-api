///VERSION DE NODE REQUERIDA
node v20.9.0 (npm v10.1.0)

///EJECUTAMOS EL COMANDO PARA INSTALAR DEPENDENCIAS
yarn

///INSTALAR LOS SIGUIENTES PAQUETES SI NO SE ENCUENTRAN EN EL pakage.json
yarn add @nestjs/swagger
yarn add @prisma/client
yarn add joi
yarn add @nestjs/config
yarn add bcrypt
yarn add passport
yarn add @nestjs/passport
yarn add class-validator
yarn add @nestjs/jwt
yarn add passport-local
yarn add passport-jwt
yarn add class-transformer
yarn add prisma-pagination
yarn add mongodb

///CREAMOS EL ARCHIVO .env Y LE AGREGAMOS LOS PARAMETROS DE CONFIGURACION
APP_NAME="Pokemon"
APP_URL=http://localhost:3000
PORT=3000
DATABASE_URL="mongodb+srv://USUARIO:PASSWORD@cluster0.xgnaoyy.mongodb.net/BASEDATOSNAME"
JWT_SECRET_KEY="P#XrJ&ayp2qP89Zuyumad!Vn4U^w^Pc2!FmvzKfrcaqc"
JWT_EXPIRATION_TIME="90d"
JWT_EXPIRATION_TIME_RECOVER_PASSWORD="300s"

//AGREGAMOS EL COMANDO AL pakage.json
"prisma": {
"seed": "ts-node prisma/seed.ts"
},

///GENERAMOS LA BD E INSERTAMOS LOS DATOS POR DEFECTO
npx prisma generate
npx prisma db push
npx prisma db seed

///EJECUTAMOS EL COMANDO PARA CORRER EL PROYECTO
yarn start:dev

///LA DOCUMENTACION DE SWAGGER ES LA SIGUIENTE
http://localhost:3000/api/docs

///PARA EJECUTAR LAS PRUEBAS SE USA EL COMANDO
npm jest

///USUARIOS DE PRUEBAS
user: "test@gmail.com"
password: 123456789

user: "admin@gmail.com"
password: 123456789
